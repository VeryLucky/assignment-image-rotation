#pragma once

#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>

struct pixel { __attribute__((packed))
    uint8_t components[3];
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image get_empty_image(uint64_t width, uint64_t height);
struct pixel* get_pixel_in_image(struct image const img, size_t width, size_t height);
void free_image(struct image image);
void rotate_image(struct image const source, struct image* rotated_image);

