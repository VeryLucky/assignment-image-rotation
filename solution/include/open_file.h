#pragma once

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_WRONG
};
static const char* const open_file_error_msg[] = {
        [OPEN_OK] = "Opening the file was successful\n",
        [OPEN_WRONG] = "Opening the file wasn't successful\n"
};

enum open_status open_file (FILE** file, const char file_path[], const char* mode);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_WRONG
};

static const char* const close_file_error_msg[] = {
        [CLOSE_OK] = "Closing the file was successful\n",
        [CLOSE_WRONG] = "Closing the file wasn't successful\n"
};

enum close_status close_file (FILE** file);

