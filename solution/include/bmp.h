#pragma once

#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_IMAGE,
    READ_INVALID_HEADER
};

static const char* const read_bmp_error_msg[] = {
        [READ_OK] = "Reading bmp was successful\n",
        [READ_INVALID_IMAGE] = "Reading image wasn't successful\n",
        [READ_INVALID_HEADER] = "Reading header wasn't successful\n"
};

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

static const char* const write_bmp_error_msg[] = {
        [WRITE_OK] = "Writing bmp was successful\n",
        [WRITE_ERROR] = "Writing bmp wasn't successful\n"
};

enum write_status to_bmp( FILE* out, struct image const* img );
