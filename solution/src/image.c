#include "../include/image.h"

struct image get_empty_image(uint64_t width, uint64_t height) {
    struct pixel* data = malloc(width * sizeof(struct pixel) * height);
    return (struct image) {width, height, data};
}

struct image get_empty_rotated_image(uint64_t width, uint64_t height) {
    return get_empty_image(height, width);
}

struct pixel* get_pixel_in_image(struct image const img, size_t width, size_t height) {
    return &img.data[img.width * height + width];
}

void rotate_image(struct image const source, struct image* rotated_image) {

    *rotated_image = get_empty_rotated_image(source.width, source.height);
    for (size_t i = 0; i < source.width; i++) {
        for (size_t j = 0; j < source.height; j++) {
            *get_pixel_in_image(*rotated_image, source.height - j -1, i) = *get_pixel_in_image(source, i, j);
        }
    }
}

void free_image(struct image image) {
    free(image.data);
}

