#include "../include/open_file.h"

enum open_status open_file (FILE** file, const char file_path[], const char* mode) {
    *file = fopen(file_path, mode);
    if (*file != NULL) return OPEN_OK;
    else return OPEN_WRONG;
}

enum close_status close_file (FILE** file) {
    int status = fclose(*file);
    if (status == EOF) return CLOSE_WRONG;
    else return CLOSE_OK;
}
