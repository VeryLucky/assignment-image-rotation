#include "../include/bmp.h"
#include "../include/open_file.h"
#include "../include/rotate.h"


int rotate(char* input, char* output) {
    FILE* f = {0};
    FILE* f1 = {0};
    struct image struct_image = {0};
    struct image rotated_image = {0};

    enum open_status file_status = open_file(&f, input, "rb");

    if (file_status != OPEN_OK) {
        fprintf(stderr, "%s", open_file_error_msg[file_status]);
        return file_status;
    } else fprintf(stdout, "%s", open_file_error_msg[file_status]);

    enum read_status status = from_bmp(f, &struct_image);
    if (status != READ_OK) {
        fprintf(stderr, "%s", read_bmp_error_msg[status]);
        close_file(&f);
        return status;
    }
    else fprintf(stdout, "%s", read_bmp_error_msg[status]);

    enum close_status close_status = close_file(&f);
    if (close_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_file_error_msg[close_status]);
        free_image(struct_image);
        return close_status;
    }
    else fprintf(stdout, "%s", close_file_error_msg[close_status]);

    rotate_image(struct_image, &rotated_image);

    enum open_status file1_status = open_file(&f1, output, "wb");
    if (file1_status != OPEN_OK) {
        fprintf(stderr, "%s", open_file_error_msg[file1_status]);
        return file1_status;
    } else fprintf(stdout, "%s", open_file_error_msg[file1_status]);

    enum write_status status1 = to_bmp(f1, &rotated_image);
    if (status1 != WRITE_OK) {
        fprintf(stderr, "%s",write_bmp_error_msg[status1]);
        close_file(&f1);
        return status1;
    }
    else fprintf(stdout, "%s",write_bmp_error_msg[status1]);

    enum close_status close1_status = close_file(&f1);
    if (close1_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_file_error_msg[close1_status]);
        free_image(rotated_image);
        return close1_status;
    }
    else fprintf(stdout, "%s", close_file_error_msg[close1_status]);

    free_image(struct_image);
    free_image(rotated_image);
    return 0;
}
