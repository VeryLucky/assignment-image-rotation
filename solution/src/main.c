#include "../include/rotate.h"
#include <stdio.h>

void usage() {
    fprintf(stderr,
            "Usage: ./image-transformer <source-image> <transformed-image>\n");
}

int main( int argc, char** argv ) {

    if (argc == 3) {
        return rotate(argv[1], argv[2]);
    } else usage();

    return 1;
}
