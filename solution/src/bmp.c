#include "../include/bmp.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static uint8_t get_bytes_to_pass (uint64_t width) {
    const uint8_t bytes_to_pass = 4 - (width * sizeof(struct pixel)) % 4;
    if (bytes_to_pass == 4) return 0;
    else return bytes_to_pass;
}

static struct bmp_header bmp_header_generation (uint64_t width, uint64_t height) {
    const uint32_t biFileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel);
    const uint32_t biSizeImage = width * height * sizeof(struct pixel);
    return (struct bmp_header) {19778, biFileSize,
                                0,54,40,
                                width, height,0,24,0,
                                biSizeImage,0,0,0,0};
}

static enum read_status check_header (struct bmp_header bmp_header) {
    if (bmp_header.bfType != 19778 && bmp_header.bOffBits != 54 &&
        bmp_header.biBitCount != 24 && bmp_header.biSize != 40) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_status read_header(FILE* input, struct bmp_header* bmp_header) {
    const size_t status = fread(bmp_header, sizeof(struct bmp_header), 1, input);
    if (status == 1) return READ_OK;
    else return READ_INVALID_HEADER;
}

static enum write_status write_header(FILE* output, struct bmp_header bmp_header) {
    const size_t status = fwrite(&bmp_header, sizeof(bmp_header), 1, output);
    if (status != 1) return WRITE_ERROR;
    return WRITE_OK;
}

static enum read_status read_image (FILE* file, struct image* img) {
    const uint8_t bytes_to_pass = get_bytes_to_pass(img->width);
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            if (fread(get_pixel_in_image(*img, j, i), sizeof(*get_pixel_in_image(*img, j, i)), 1, file) != 1) return READ_INVALID_IMAGE;
        }
        if (fseek(file, bytes_to_pass, SEEK_CUR) != 0) return READ_INVALID_IMAGE;
    }
    return READ_OK;
}

static enum write_status write_image (FILE* file, struct image img) {
    const uint8_t bytes_to_pass = get_bytes_to_pass(img.width);
    const int64_t o = 0;
    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            if (fwrite(get_pixel_in_image(img, j, i), sizeof(*get_pixel_in_image(img, j, i)), 1, file) != 1) return WRITE_ERROR;
        }
        if (i != img.height - 1)
            if (fwrite(&o, 1, bytes_to_pass, file) != bytes_to_pass) return WRITE_ERROR;
    }
    if (fwrite(&o, 1, bytes_to_pass, file) != bytes_to_pass) return WRITE_ERROR;
    return WRITE_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header bmp_header = {0};
    const enum read_status bmp_header_status = read_header(in, &bmp_header);
    if (bmp_header_status != READ_OK) return bmp_header_status;
    const enum read_status checking_bmp_header_status = check_header(bmp_header);
    if (checking_bmp_header_status != READ_OK) return checking_bmp_header_status;
    *img = get_empty_image(bmp_header.biWidth, bmp_header.biHeight);
    enum read_status status = read_image(in, img);
    if (status != READ_OK) free_image(*img);
    return status;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    const struct bmp_header bmp_header = bmp_header_generation(img->width, img->height);
    enum write_status status =  write_header(out, bmp_header);
    if (status != WRITE_OK) {
        free_image(*img);
        return status;
    }
    status = write_image(out, *img);
    if (status != WRITE_OK) free_image(*img);
    return status;
}





